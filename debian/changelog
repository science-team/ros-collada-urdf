ros-collada-urdf (1.12.13-11) unstable; urgency=medium

  [ Jose Luis Rivero ]
  * [PATCH] Fix tests: pr2 XML declaration wrong

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 05 Nov 2024 15:28:01 +0100

ros-collada-urdf (1.12.13-10) unstable; urgency=medium

  * Wrap and sort Debian package files
  * Migrate to dh-ros
  * Add myself to uploaders

 -- Timo Röhling <roehling@debian.org>  Fri, 04 Oct 2024 01:13:44 +0200

ros-collada-urdf (1.12.13-9) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Jose Luis Rivero ]
  * Include a patch for compiling against urdfdom 4.x

  [ Jochen Sprickerhof ]
  * Cleanup after build (Closes: #1045190)
  * Bump policy version (no changes)

  [ Helmut Grohne ]
  * Fix FTCBFS: Missing dependency on pkgconf. (Closes: #1078045)

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 01 Sep 2024 14:08:38 +0200

ros-collada-urdf (1.12.13-8) unstable; urgency=medium

  * Drop nose dependency (Closes: #1018623)
  * Bump policy version (no changes)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 29 Aug 2022 14:36:18 +0200

ros-collada-urdf (1.12.13-7) unstable; urgency=medium

  * Added patch for log4cxx

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Wed, 02 Feb 2022 11:28:54 +0100

ros-collada-urdf (1.12.13-6) unstable; urgency=medium

  * Enable tests

 -- Jochen Sprickerhof <jspricke@debian.org>  Wed, 27 Oct 2021 08:05:19 +0200

ros-collada-urdf (1.12.13-5) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Timo Röhling <roehling@debian.org>  Wed, 22 Sep 2021 22:41:54 +0200

ros-collada-urdf (1.12.13-4) experimental; urgency=medium

  * Team upload.
  * Move pkg-config and CMake config files to /usr/lib/<triplet>
  * Bump Standards-Version to 4.6.0 (no changes)

 -- Timo Röhling <roehling@debian.org>  Mon, 20 Sep 2021 22:39:51 +0200

ros-collada-urdf (1.12.13-3) unstable; urgency=medium

  * Add Breaks+Replaces for old libcollada-parser-dev.
    Thanks to Andreas Beckmann (Closes: #977906)

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 22 Dec 2020 18:51:02 +0100

ros-collada-urdf (1.12.13-2) unstable; urgency=medium

  * Fix package name in d/watch
  * simplify packaging

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 19 Dec 2020 16:26:45 +0100

ros-collada-urdf (1.12.13-1) unstable; urgency=medium

  * New upstream version 1.12.13
  * rebase patches
  * bump debhelper version

 -- Jochen Sprickerhof <jspricke@debian.org>  Thu, 16 Jul 2020 15:50:27 +0200

ros-collada-urdf (1.12.12-3) unstable; urgency=medium

  * Update collada-dom dependency to its new name
  * Added hardening flags
  * Bumped standards version to 4.5.0. No changes

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Wed, 18 Mar 2020 23:24:52 +0100

ros-collada-urdf (1.12.12-2) unstable; urgency=medium

  * Source only upload

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 09 Nov 2019 22:19:19 +0100

ros-collada-urdf (1.12.12-1) unstable; urgency=medium

  * Initial release, split from ros-robot-model (Closes: #903649).

 -- Leopold Palomo-Avellaneda <leo@alaxarxa.net>  Sun, 27 Oct 2019 09:46:01 +0100
